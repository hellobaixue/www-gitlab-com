title: Hemmersbach
cover_image: '/images/blogimages/hemmersbach_case_study.jpg'
cover_title: |
  Hemmersbach reorganized their build chain and increased build speed 59x
cover_description: |
  Eliminated 1 week of cycle time and increased ideas into production by 31 percent
twitter_image: '/images/blogimages/hemmersbach_case_study.jpg'

customer_logo: 'images/case_study_logos/hemmersbach_logo_orange.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Germany, Poland, Hungary, Bulgaria, Austria
customer_solution: GitLab Ultimate
customer_employees: 3,400 employees
customer_overview: |
   Hemmersbach is a service provider for the IT industry. The company is responsible 
   for providing repair and service for some of the largest hardware manufactures in 
   the world with 38 subsidiaries and operates in more than 190 countries.
customer_challenge: |
  Hemmersbach was burdened by multiple tools and communication inefficiencies. 
  This resulted in slow production builds and manual processes. 

key_benefits:
  - |
    14.4% improvement in cycle time 
  - |
    Idea to production dropped from 45 days to 38.5- 31% increase
  - |
    Completed 212 ideas to production with GitLab - an increase of 31%

customer_stats:
  - stat: 60 
    label: Builds per day
  - stat: 530
    label: Automated tests daily
  - stat: 30
    label: Automated daily deploys

customer_study_content:
  - title: the customer
    subtitle: Providing IT support on a global scale
    content:
      - |
        Hemmersbach provides vital support for the leading companies in the IT industry. 
        The company offers global services for manufacturers and outsourcing companies to 
        meet the wide variety of needs of their end customers. They provide technology 
        services for thousands of organizations and specialize in supporting hardware 
        of all sizes- from printers to the largest servers. 
        


  - title: the challenge
    subtitle: Collaboration and speed at a global scale
    content:
      - |
        Hemmersbach has three teams working on the company’s proprietary software. A few years ago, 
        they faced challenges around these teams working disparately. They struggled with collaboration 
        and geography— with speed suffering as a result. They were using a scrum framework with a 
        toolchain that included Jira, Jenkins, Tulip, and a variety of open source tools. With this 
        complex toolchain, teams lacked traceability and had to overcome inefficiencies.
      - |
        “Most of our code was not connected to our process,” explained Alexander Schmid, Head of 
        Software Development. “We had tools and repositories and all the merge request stuff. But, 
        these things were not related to what guys wrote on comments and the stuff it was not connected.”
      - |
        Struggling with multiple tools, difficult build cycles, and an out of sync planning process in 
        Jira, Hemmersbach needed a new process. They tried a wide variety of tools to solve the dilemma 
        but still couldn't achieve the speed they desired. During this time, teams were using GitLab Core 
        and the business was impressed by how connected issues are with code repositories and how 
        GitLab helped them improve team efficiency. 


  - title: the solution
    subtitle: Rebuilding the wheel to achieve speed and collaboration
    content:
      - |
        The Hemmersbach development group evaluated their process and overcame the challenges they were facing 
        by rethinking how they worked together. They had a team utilizing GitLab and were impressed with how 
        everything is connected to the commit and offers an overview for all of the pipeline steps — in one tool. 
      - |
        GitLab Core provided built-in CI/CD, and project issue boards and teams were able to increase build speed 
        and collaboration. Building on this momentum, they decided to rebuild the entire structure and move from 
        scrum development to feature-driven development. While they saw progress, they still needed a tool that 
        offered the ability to categorize items and then break them down so the team could continue to iterate faster. 
      - | 
        Gitlab Ultimate connected the portfolio planning process with the actual work to deliver new capabilities. The 
        multi-level epics and roadmap review capabilities powered their teams to move at unprecedented speed. The code 
        reviews and comments in GitLab helped improve overall code quality.

  - blockquote: We renewed the complete process. We didn't just take what was in Jenkins and move to GitLab, we redid everything. We took all of the findings we had from Jenkins, which wasn’t good, and we took what was good and we set it up completely new. We reinvented the wheel, just to make it better.
    attribution: Alexander Schmid
    attribution_title: Head of Software Development

  - title: the results 
    subtitle: Removing a week from idea to production time
    content:
      - |
        Using GitLab helped Hemmersbach decreased the time from planning to production by 6.5 days. By working in a 
        single environment, they are also achieving 60 builds per day when previously they were only performing a single daily build.
        
  - blockquote: GitLab is the one tool that connects our whole team. You always see GitLab open and everything is based on GitLab. GitLab is the backbone of our software development.
    attribution: Alexander Schmid
    attribution_title: Head of Software Development
    content:
      - |
        Hemmersbach has over 100 developers using GitLab on a daily basis to run their software. The deployments now begin with 
        Epics which allow everyone to collaborate as they work through the resulting issues and merge requests. Having all of the 
        collaboration capabilities under one umbrella has enabled the company to bring teams together and achieve unprecedented deployment speed. 
      - |
        “Everybody can see what's going on across other projects as well,” Schmid said. “And then if there are comments or concerns with the code, 
        you just write it, you just do it. GitLab has helped us bring teams together.”
